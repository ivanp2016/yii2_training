<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "gender".
 *
 * @property string $id
 * @property string $name_gen
 */
class Gender extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gender';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_gen'], 'required'],
            [['name_gen'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_gen' => 'Name Gen',
        ];
    }
    public function getGender()
    {
        $gender = self::find()
            ->select('name_gender')
            ->all();
        return $gender;
    }
    
}

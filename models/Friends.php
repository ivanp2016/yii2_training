<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "friends".
 *
 * @property string $id
 * @property string $user_id
 * @property string $friend_user
 *
 * @property Users $user
 * @property Users $friendUser
 */
class Friends extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'friends';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'friend_user'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['friend_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['friend_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'friend_user' => 'Friend User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFriendUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'friend_user']);
    }
    public function getFriends()
    {

        $friends = self::find()
            ->where(['id'=>1])
            ->all();

        return $friends;

    }
//    public function getUsers_rel() {
//        return $this->hasOne(Users::className(), ['id' => 'user_id']);
//    }
    public function getAllFriends()
    {

        $friends = Friends::find()
            ->select([ 'friends.user_id', 'friends.friend_user' ])
//            ->leftJoin('users','users.id = friends.friend_user')
            ->where(['friends.user_id' => 1])
            ->with('friendUser')
            ->all();
        return $friends;
    }

    public function arrayUsers()
    {
        $users = self::find()
            ->select('user_id')
            ->distinct('user_id')
            ->all();
        return $users;
    }

    public function countFriends($id)
    {
        $friends = self::find()
            ->select('user_id')
            ->where(['user_id' => $id])
            ->count();
        return $friends;
    }

    public function nameFriends($user)
    {
        $names = self::find()
            ->select(['friends.user_id', 'friends.friend_user'])
            ->where(['friends.user_id' => $user])
            ->with('friendUser')
//            ->asArray()
            ->all();
         return $names;   
    }

    public function friendsUsers($id)
    {
        $friends_user = self::find()
            ->where(['user_id' => $id])
            ->all();
        return $friends_user;
    }

    public function CheckUser($id)
    {
        $status = self::findOne($id);
        return $status;
    }
//update
    public function friendsCheck($id_user, $id_friend)
    {
        $friends_check = self::find()
            ->where(['user_id' => $id_user])
            ->andWhere(['friend_user' => $id_friend])
            ->exists();
        return $friends_check;                    
    }    
//save
    public function saveFriend($id_user, $id_friend)
    {
        $new_friend = new Friends();
        $new_friend->user_id = $id_user;
        $new_friend->friend_user = $id_friend;
        $new_friend->save();
    }
//delete
    public function clearFriends($id_user, $id_friend)
    {
        $findId = self::find()
            ->where(['user_id' => $id_user])
            ->andWhere(['friend_user' => $id_friend])
            ->one();


        self::findOne($findId->id)
            ->delete();
    }
//processor data
    public function processor($datas)
    {
        $prepare = array();
        foreach ($datas as $data) {
            array_push($prepare, $data->friendUser->name);
        }
        return $prepare;
    }
    public function processorFriends($datas){
        $prepare = array();
       
        foreach ($datas as $data) {
                array_push($prepare, ['user' => $data->user_id,'friend' => $data->friend_user]);
            }

        return $prepare;
    }

    public function processorUsers($datas)
    {
        $prepare = array();

        foreach ($datas as $data) {
            array_push($prepare, ['id' => $data->id,'name' => $data->name]);
        }

        return $prepare;
    }
}

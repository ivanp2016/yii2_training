<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property string $name
 * @property string $gender_id
 * @property integer $age
 * @property integer $friend_id
 *
 * @property Friends[] $friends
 * @property Friends[] $friends0
 * @property Gender $gender
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'gender_id', 'age', 'friend_id'], 'required'],
            [['gender_id', 'age', 'friend_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['gender_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gender::className(), 'targetAttribute' => ['gender_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'gender_id' => 'Gender ID',
            'age' => 'Age',
            'friend_id' => 'Friend ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFriends()
    {
        return $this->hasMany(Friends::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFriends0()
    {
        return $this->hasMany(Friends::className(), ['friend_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGender()
    {
        return $this->hasOne(Gender::className(), ['id' => 'gender_id']);
    }
    public function getUsers()
    {
        $friends = self::find()
            ->orderBy('id')
            ->all();
        return ($friends)? $friends:0;
    }

    public function saveNewUser($name, $gender, $age)
    {
        $users = new Users();
        $users->name = $name;
        $users->gender_id = $gender;
        $users->age = $age;
        $users->friend_id = 0;
        $users->save();
    }
    public function updateUser($id, $name, $gender, $age)
    {
        $users = Users::findOne($id);
        $users->name = $name;
        $users->gender_id = $gender;
        $users->age = $age;
        $users->update();
    }
    public function deleteUser($id)
    {
        $users = self::findOne($id);
        $users->delete();
    }
}

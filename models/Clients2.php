<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients2".
 *
 * @property string $id
 * @property string $email
 * @property string $date
 * @property string $sum
 * @property string $current_date
 * @property string $different_day
 */
class Clients2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['current_date'], 'safe'],
            [['different_day'], 'integer'],
            [['email', 'date', 'sum'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'date' => 'Date',
            'sum' => 'Sum',
            'current_date' => 'Current Date',
            'different_day' => 'Different Day',
        ];
    }
    public static function SaveClients2($sum, $date, $email, $different)
    {
        $clients = new Clients2();
        $clients->email = $email;
        $clients->date = $date;
        $clients->sum = $sum;
        $clients->different_day = $different;
        $clients->save();
    }
}

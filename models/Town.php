<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;
use yii\data\Pagination;

/**
 * This is the model class for table "town".
 *
 * @property string $id
 * @property string $country
 * @property string $name_town
 * @property integer $peaples
 */
class Town extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'town';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'name_town', 'peaples'], 'required'],
            [['peaples'], 'integer'],
            [['country', 'name_town'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country' => 'Country',
            'name_town' => 'Name Town',
            'peaples' => 'Peaples',
        ];
    }

    public function getTown()
    {
        $town = self::find()
            ->orderBy('id')
            ->all();
        
        return ($town)?$town:0;
    }
    public function sortPeoples()
    {
        $town = self::find()
            ->orderBy('peaples DESC')
            ->all();
        return ($town)?$town:0;
    }
    public function sortTowns()
    {
        $town = self::find()
            ->orderBy('name_town')
            ->all();

        return ($town)?$town:0;
    }
    public function saveForm($country, $town, $peoples)
    {
        $towns = new Town();
        $towns->country = $country;
        $towns->name_town = $town;
        $towns->peaples = $peoples;
        $towns->save();
    }
    public function pager(&$countries, &$pagination)
    {
        $query = Town::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);


        $countries = $query->orderBy('name_town')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        
        return $countries;
    }
}

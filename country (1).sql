-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 26 2016 г., 20:46
-- Версия сервера: 5.5.45
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `country`
--

-- --------------------------------------------------------

--
-- Структура таблицы `clients2`
--

CREATE TABLE IF NOT EXISTS `clients2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL DEFAULT '0',
  `date` varchar(50) NOT NULL DEFAULT '0',
  `sum` varchar(50) NOT NULL DEFAULT '0',
  `current_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `different_day` int(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=122 ;

--
-- Дамп данных таблицы `clients2`
--

INSERT INTO `clients2` (`id`, `email`, `date`, `sum`, `current_date`, `different_day`) VALUES
(1, 'vera_sanzhimitupova@mail.ru\r\n', '11.05.2016', '30000', '2016-05-25 15:38:57', 14),
(2, 'matrena-konstantinova@mail.ru\r\n', '12.05.2016', '5000', '2016-05-25 15:38:57', 13),
(3, 'tnoeva@list.ru\r\n', '12.05.2016', '5000', '2016-05-25 15:38:57', 13),
(4, 'slepaleksandra@mail.ru\r\n', '11.05.2016', '5000', '2016-05-25 15:38:57', 14),
(5, 'doktorov.66@mail.ru\r\n', '14.05.2016', '30000', '2016-05-25 15:38:57', 11),
(6, 'danstaskevic@mail.ru\r\n', '14.05.2016', '5000', '2016-05-25 15:38:57', 11),
(7, 'emirkereitov@gmail.com\r\n', '10.05.2016', '70000', '2016-05-25 15:38:57', 15),
(8, 'u.Larka@mail.ru\r\n', '12.05.2016', '80000', '2016-05-25 15:38:57', 13),
(9, 'krispanda111@gmail.com\r\n', '13.05.2016', '450000', '2016-05-25 15:38:57', 12),
(10, 'Simon_art69@mail.ru\r\n', '13.05.2016', '50000', '2016-05-25 15:38:57', 12),
(11, 'matann1965@mail.ru\r\n', '17.05.2016', '10000', '2016-05-25 15:38:57', 8),
(12, 'arturellyaev@mail.ru\r\n', '17.05.2016', '500000', '2016-05-25 15:38:57', 8),
(13, 'erkinnik99@mail.ru\r\n', '17.05.2016', '20000', '2016-05-25 15:38:57', 8),
(14, 'yurisiv98@mail.ru\r\n', '17.05.2016', '5000', '2016-05-25 15:38:57', 8),
(15, 'alenatim199918@gmail.com\r\n', '17.05.2016', '20000', '2016-05-25 15:38:57', 8),
(16, 'mironertyukov@list.ru\r\n', '10.05.2016', '300000', '2016-05-25 15:38:57', 15),
(17, 'ttorg72@mail.ru\r\n', '12.05.2016', '500000', '2016-05-25 15:38:57', 13),
(18, 'mr.rodomir@mail.ru\r\n', '17.05.2016', '50000', '2016-05-25 15:38:57', 8),
(19, 'nadikkaa84@mail.ru\r\n', '17.05.2016', '500000', '2016-05-25 15:38:57', 8),
(20, 'anya.kychkina@mail.ru\r\n', '17.05.2016', '300000', '2016-05-25 15:38:57', 8),
(21, 'popovnik2301@mail.ru\r\n', '16.05.2016', '300000', '2016-05-25 15:38:57', 9),
(22, 'ulya.ayanitova@mail.ru\r\n', '16.05.2016', '20000', '2016-05-25 15:38:57', 9),
(23, 'arin.pav@mail.ru\r\n', '16.05.2016', '5000', '2016-05-25 15:38:57', 9),
(24, 'sajnaarakorakina@gmail.com\r\n', '16.05.2016', '50000', '2016-05-25 15:38:57', 9),
(25, 'klavdiya.nikolaeva.49@mail.ru\r\n', '17.05.2016', '100000', '2016-05-25 15:38:57', 8),
(26, 'mikhail.efimov.64@mail.ru\r\n', '10.05.2016', '50000', '2016-05-25 15:38:57', 15),
(27, 'PNIKOLAY90@mail.ru\r\n', '11.05.2016', '450000', '2016-05-25 15:38:57', 14),
(28, 'ildora75@mail.ru\r\n', '17.05.2016', '50000', '2016-05-25 15:38:57', 8),
(29, 'tanya_nik_1983@mail.ru\r\n', '15.05.2016', '500000', '2016-05-25 15:38:57', 10),
(30, 'nov68darya@mail.ru\r\n', '17.05.2016', '30000', '2016-05-25 15:38:57', 8),
(31, 'siv86marfa@mail.ru\r\n', '17.05.2016', '30000', '2016-05-25 15:38:57', 8),
(32, 'makar.sv2016@yandex.ru\r\n', '17.05.2016', '450000', '2016-05-25 15:38:57', 8),
(33, 'vas.makarova99@mail.ru\r\n', '17.05.2016', '5000', '2016-05-25 15:38:57', 8),
(34, 'radnaev150489@mail.ru\r\n', '17.05.2016', '5000', '2016-05-25 15:38:57', 8),
(35, 'Argakovaalena@mail.ru\r\n', '17.05.2016', '350000', '2016-05-25 15:38:57', 8),
(36, 'alinka0187@mail.ru\r\n', '17.05.2016', '350000', '2016-05-25 15:38:57', 8),
(37, 'arsan-rektime@mail.ru\r\n', '17.05.2016', '350000', '2016-05-25 15:38:57', 8),
(38, 'Yaroi777ykt@mail.ru\r\n', '18.05.2016', '350000', '2016-05-25 15:38:57', 7),
(39, 'a88romanov@gmail.com\r\n', '18.05.2016', '100000', '2016-05-25 15:38:57', 7),
(40, 'pasha_10.12@mail.ru\r\n', '18.05.2016', '50000', '2016-05-25 15:38:57', 7),
(41, 'shurskayas@mail.ru\r\n', '18.05.2016', '500000', '2016-05-25 15:38:57', 7),
(42, 'nne2751@mail.ru\r\n', '18.05.2016', '10000', '2016-05-25 15:38:57', 7),
(43, 'milena14062000@icloud.com\r\n', '18.05.2016', '50000', '2016-05-25 15:38:57', 7),
(44, 'lanatur1952@mail.ru\r\n', '18.05.2016', '10000', '2016-05-25 15:38:57', 7),
(45, 'ylapetrova01@gmail.com\r\n', '18.05.2016', '50000', '2016-05-25 15:38:57', 7),
(46, 'alexandra.60@inbox.ru\r\n', '16.05.2016', '100000', '2016-05-25 15:38:57', 9),
(47, 'bachka_1011@yahoo.com\r\n', '16.05.2016', '350000', '2016-05-25 15:38:57', 9),
(48, 'ant.romanova2015@yandex.ru\r\n', '12.05.2016', '200000', '2016-05-25 15:38:57', 13),
(49, 'Stepanofpmed@gmail.com\r\n', '12.05.2016', '70000', '2016-05-25 15:38:57', 13),
(50, 'env-ykt@yandex.ru\r\n', '13.05.2016', '450000', '2016-05-25 15:38:57', 12),
(51, 'Mahalvovna_89@mail.ru\r\n', '14.05.2016', '70000', '2016-05-25 15:38:57', 11),
(52, 'khpan@mail.ru\r\n', '16.05.2016', '500000', '2016-05-25 15:38:57', 9),
(53, 'aprok000@gmail.com\r\n', '18.05.2016', '350000', '2016-05-25 15:38:57', 7),
(54, 'anastasianazarova6@gmail.com\r\n', '18.05.2016', '200000', '2016-05-25 15:38:57', 7),
(55, 'fedotovasar1980@gmail.com\r\n', '18.05.2016', '450000', '2016-05-25 15:38:57', 7),
(56, 'kyoregei87@mail.ru\r\n', '19.05.2016', '200000', '2016-05-25 15:38:57', 6),
(57, 'nurgunneustroev@mail.ru\r\n', '19.05.2016', '450000', '2016-05-25 15:38:57', 6),
(58, 'Femalestrv@mail.ru\r\n', '19.05.2016', '200000', '2016-05-25 15:38:57', 6),
(59, 'valentin-kolod2016@yandex.ru\r\n', '19.05.2016', '230000', '2016-05-25 15:38:57', 6),
(60, 'yurchenko-alla@list.ru\r\n', '19.05.2016', '50000', '2016-05-25 15:38:57', 6),
(61, 'sivtsevaTV74@mail.ru\r\n', '19.05.2016', '60000', '2016-05-25 15:38:57', 6),
(62, 'antonovaZM85@mail.ru\r\n', '19.05.2016', '150000', '2016-05-25 15:38:57', 6),
(63, 'vinraykon@mail.ru\r\n', '19.05.2016', '50000', '2016-05-25 15:38:57', 6),
(64, 'argunovaLP64@mail.ru\r\n', '19.05.2016', '5000', '2016-05-25 15:38:57', 6),
(65, 'sidorovAM55@mail.ru\r\n', '16.05.2016', '50000', '2016-05-25 15:38:57', 9),
(66, 'albina-stepanova78@mail.ru\r\n', '19.05.2016', '80000', '2016-05-25 15:38:57', 6),
(67, 'anna.polyatinskaya@gmail.com\r\n', '19.05.2016', '250000', '2016-05-25 15:38:57', 6),
(68, 'petrsiv78@mail.ru\r\n', '19.05.2016', '150000', '2016-05-25 15:38:57', 6),
(69, 'impossiblek21@mail.ru\r\n', '18.05.2016', '5000', '2016-05-25 15:38:57', 7),
(70, 'niknikzakharov@mail.ru\r\n', '18.05.2016', '400000', '2016-05-25 15:38:57', 7),
(71, 'lekhanoev1989@mail.ru\r\n', '18.05.2016', '350000', '2016-05-25 15:38:57', 7),
(72, 'Mak_Sakhaya@mail.ru\r\n', '18.05.2016', '90000', '2016-05-25 15:38:57', 7),
(73, 'Lenochkazakharova@mail.ru\r\n', '19.05.2016', '10000', '2016-05-25 15:38:57', 6),
(74, 'I.fedorov1@icloud.com\r\n', '19.05.2016', '350000', '2016-05-25 15:38:57', 6),
(75, 'Vinok_09@mail.ru\r\n', '19.05.2016', '130000', '2016-05-25 15:38:57', 6),
(76, 'Agnessa_Ayanessa2799@mail.ru\r\n', '19.05.2016', '255000', '2016-05-25 15:38:57', 6),
(77, 'petrushin.1964@inbox.ru\r\n', '19.05.2016', '5000', '2016-05-25 15:38:57', 6),
(78, 'gavril.malyshev@mail.ru\r\n', '19.05.2016', '200000', '2016-05-25 15:38:57', 6),
(79, 'darina.fedorova.2001@mail.ru\r\n', '19.05.2016', '30000', '2016-05-25 15:38:57', 6),
(80, 'mzhegusova@mail.ru\r\n', '19.05.2016', '100000', '2016-05-25 15:38:57', 6),
(81, 'pnik2@bk.ru\r\n', '19.05.2016', '350000', '2016-05-25 15:38:57', 6),
(82, 'innokentii_kolod@mail.ru\r\n', '19.05.2016', '350000', '2016-05-25 15:38:57', 6),
(83, 'nsm2751@mail.ru\r\n', '19.05.2016', '10000', '2016-05-25 15:38:57', 6),
(84, 'shanna1956@mail.ru\r\n', '19.05.2016', '10000', '2016-05-25 15:38:57', 6),
(85, 'zapivaloff.dima@yandex.ru\r\n', '19.05.2016', '110000', '2016-05-25 15:38:57', 6),
(86, 'Saaska92@mail.ru\r\n', '19.05.2016', '140000', '2016-05-25 15:38:57', 6),
(87, 'natasha.akimova82@mail.ru\r\n', '19.05.2016', '450000', '2016-05-25 15:38:57', 6),
(88, 'PestryakBB@mail.ru\r\n', '19.05.2016', '60000', '2016-05-25 15:38:57', 6),
(89, 'valentinaser1962@mail.ru\r\n', '10.05.2016', '100000', '2016-05-25 15:38:57', 15),
(90, '864778bochuonai@gmail.com\r\n', '11.05.2016', '450000', '2016-05-25 15:38:57', 14),
(91, 'elinaedgar2009@gmail.com\r\n', '11.05.2016', '250000', '2016-05-25 15:38:57', 14),
(92, 'Tyskyl1@yandex.ru\r\n', '13.05.2016', '500000', '2016-05-25 15:38:57', 12),
(93, 'pushkina_26@mail.ru\r\n', '16.05.2016', '300000', '2016-05-25 15:38:57', 9),
(94, 'ciriusla86@mail.ru\r\n', '17.05.2016', '500000', '2016-05-25 15:38:57', 8),
(95, 'Olga.uarova.88@mail.ru\r\n', '17.05.2016', '250000', '2016-05-25 15:38:57', 8),
(96, 'GSE16@mail.ru\r\n', '17.05.2016', '350000', '2016-05-25 15:38:57', 8),
(97, 'agogol7013@gmail.com\r\n', '17.05.2016', '50000', '2016-05-25 15:38:57', 8),
(98, 'tuyaratarasova1989@mail.ru\r\n', '11.05.2016', '400000', '2016-05-25 15:38:57', 14),
(99, 'alst_12@mail.ru\r\n', '11.05.2016', '20000', '2016-05-25 15:38:57', 14),
(100, '89141077080@mail.ru\r\n', '17.05.2016', '150000', '2016-05-25 15:38:57', 8),
(101, 'stas.borisov89@mail.ru\r\n', '19.05.2016', '500000', '2016-05-25 15:38:57', 6),
(102, 'v.tuyara70@mail.ru\r\n', '19.05.2016', '50000', '2016-05-25 15:38:57', 6),
(103, 'k.svet56@mail.ru\r\n', '19.05.2016', '13000', '2016-05-25 15:38:57', 6),
(104, 'mari.neustroeva.68@mail.ru\r\n', '19.05.2016', '10000', '2016-05-25 15:38:57', 6),
(105, 'ildarpavlov888@mail.ru\r\n', '19.05.2016', '350000', '2016-05-25 15:38:57', 6),
(106, 'agitovna78@mail.ru\r\n', '19.05.2016', '250000', '2016-05-25 15:38:57', 6),
(107, 'ISP1948@mail.ru\r\n', '19.05.2016', '50000', '2016-05-25 15:38:57', 6),
(108, 'lenaafan1961@mail.ru\r\n', '19.05.2016', '100000', '2016-05-25 15:38:57', 6),
(109, 'SNA03021974@mail.ru\r\n', '19.05.2016', '30000', '2016-05-25 15:38:57', 6),
(110, 'akulinalog09@mail.ru\r\n', '16.05.2016', '450000', '2016-05-25 15:38:57', 9),
(111, 'natavas.1959@mail.ru\r\n', '19.05.2016', '150000', '2016-05-25 15:38:57', 6),
(112, 'Dayaana_std@mail.ru\r\n', '19.05.2016', '10000', '2016-05-25 15:38:57', 6),
(113, 'sargilanalebedkina@mail.ru\r\n', '19.05.2016', '350000', '2016-05-25 15:38:57', 6),
(114, 'alexpti79@mail.ru\r\n', '19.05.2016', '50000', '2016-05-25 15:38:57', 6),
(115, 'ameliprokopeva2017@mail.ru\r\n', '12.05.2016', '300000', '2016-05-25 15:38:57', 13),
(116, 'streshstep@mail.ru\r\n', '12.05.2016', '450000', '2016-05-25 15:38:57', 13),
(117, 'sardandreeva138@gmail.com\r\n', '16.05.2016', '100000', '2016-05-25 15:38:57', 9),
(118, 'maknani@bk.ru\r\n', '16.05.2016', '200000', '2016-05-25 15:38:57', 9),
(119, 'ivanov-p75@yandex.ru\r\n', '19.05.2016', '450000', '2016-05-25 15:38:57', 6),
(120, 'doctor_pinigina@mail.com\r\n', '20.05.2016', '100000', '2016-05-25 15:38:57', 5),
(121, 'mscreant@mail.ru', '20.05.2016', '130000', '2016-05-25 15:38:57', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `clients_test`
--

CREATE TABLE IF NOT EXISTS `clients_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `time_registration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `clients_test`
--

INSERT INTO `clients_test` (`id`, `name`, `time_registration`) VALUES
(1, 'ivan', '2016-05-25 15:02:14'),
(2, 'Вася', '2016-05-25 15:03:31'),
(3, 'Вася', '2016-05-25 15:05:37'),
(4, 'Вася', '2016-05-25 15:11:07'),
(5, 'Вася', '2016-05-25 15:14:21'),
(6, 'Вася', '2016-05-25 15:14:22'),
(7, 'Вася', '2016-05-25 15:15:52'),
(8, 'Вася', '2016-05-25 15:33:35'),
(9, 'Вася', '2016-05-25 15:35:13');

-- --------------------------------------------------------

--
-- Структура таблицы `dating`
--

CREATE TABLE IF NOT EXISTS `dating` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `dating`
--

INSERT INTO `dating` (`id`, `data`) VALUES
(1, '0000-00-00 00:00:00'),
(2, '2016-05-19 08:17:12');

-- --------------------------------------------------------

--
-- Структура таблицы `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(5) unsigned DEFAULT NULL,
  `friend_user` int(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `friend_user` (`friend_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `friends`
--

INSERT INTO `friends` (`id`, `user_id`, `friend_user`) VALUES
(1, 1, 2),
(2, 1, 13),
(3, 2, 1),
(4, 2, 13),
(5, 13, 1),
(6, 13, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name_gender` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `gender`
--

INSERT INTO `gender` (`id`, `name_gender`) VALUES
(1, 'чоловіча'),
(2, 'жіноча');

-- --------------------------------------------------------

--
-- Структура таблицы `town`
--

CREATE TABLE IF NOT EXISTS `town` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL,
  `name_town` varchar(100) NOT NULL,
  `peaples` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `town`
--

INSERT INTO `town` (`id`, `country`, `name_town`, `peaples`) VALUES
(1, 'Україна', 'Вінниця', 500000),
(2, 'Україна', 'Київ', 3000000),
(3, 'Україна', 'Харків', 1451132),
(4, 'Україна', 'Львів', 729038),
(5, 'Україна', 'Жмеринка', 37349),
(6, 'Україна', 'Козятин', 27643),
(7, 'Україна', 'Луцьк', 208816),
(8, 'Україна', 'Дніпропетровськ', 1080846),
(9, 'Білорусь', 'Мінськ', 1922002),
(10, 'ewrwre', 'ertwet', 464644);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `gender_id` int(5) unsigned NOT NULL,
  `age` int(3) NOT NULL,
  `friend_id` int(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gender_id` (`gender_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `gender_id`, `age`, `friend_id`) VALUES
(1, 'Іван', 1, 27, 1),
(2, 'Вітя', 1, 25, 1),
(13, 'Ihor', 1, 34, 1),
(14, 'Юра', 1, 1, 2),
(15, 'Оля', 2, 21, 5);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `friend_user` FOREIGN KEY (`friend_user`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `gen_key` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

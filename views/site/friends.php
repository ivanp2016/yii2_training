<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Gender;
use yii\bootstrap\modal;
use yii\bootstrap\Dropdown;
//var_dump($array_friends);die();
//function var_object($var) {
//    static $int=0;
//    echo '<pre><b style="background: red;padding: 1px 5px;">'.$int.'</b> ';
//    var_export($var);
//    echo '</pre>';
//    $int++;
//    die();
//}
//var_object($array_friends);die();

$this->title = 'Друзі';
function getModal($user)
    {
        Modal::begin([
            'size' => 'modal-lg',
            'header' => '<h2>Редагувати</h2>',
            'toggleButton' => [
                'label' => 'Редагувати',
                'tag' => 'button',
                'class' => 'btn btn-warning',
            ],
            'footer' => ''
        ]);
        ?>
        <table>
            <thead>
                <tr>
                    <th>Ім'я</th>
                    <th>Стать</th>
                    <th>Вік</th>
                </tr>
            </thead>
            <tbody>
            <?= Html::beginForm(['site/update_u', 'user' => $user], 'post', ['enctype' => 'multipart/form-data']) ?>
                <tr>
                    <td style="display: none">
                        <?= Html::input('text',     'id',     $user->id,['class' => '']) ?>
                    </td>
                    <td>
                        <?= Html::input('text',     'name',     $user->name,['class' => '']) ?>
                    </td>
                    <td>
                        <?= Html::dropDownList('gender', null,
                            ArrayHelper::map(Gender::find('name')->all(), 'id', 'name_gender')) ?>
                    </td>
                    <td>
                        <?= Html::input('number',   'age',      $user->age,['class' => '']) ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-primary']); ?>
        <?= Html::endForm(); ?>
        <?php Modal::end(); ?>
<?php
        return null;
   }
?>
<h2><span class="label label-default">Соціальна мережа</span></h2>
<!--All data in table is attached to object User for id user-->
<table class="table">
    <thead>
        <tr>
            <th>Ім'я</th>
            <th>Стать</th>
            <th>Вік</th>
            <th>Друзі</th>
            <th>Друзі</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $key => $user): ?>
            <tr>
                <td id="<?= $user->id ?>" class="users"><?=$user->name ?></td>
                <td><?=$genders[$user->gender_id-1]->name_gender ?></td>
                <td><?=$user->age?></td>
                <td><?= Html::submitButton('Друзі', ['class' => 'btn btn-default contacts', 'id' => $user->id,
                        'data-target' => "#myModalContacts", 'data-toggle' => "modal",
                        'data-toggle' => 'tooltip', 'title' => 'Керувати контактами моїх друзів'])?></td>
                <td>
                    <div class="dropdown" id="<?= $user->id ?>">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                           <li><a href="#"></a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <?= getModal($user); ?>

                </td>
                <td>
                    <a href="<?=Url::to(['site/delete', 'id' => $user->id]) ?>"><?= Html::submitButton('Видалити', ['class' => 'btn btn-danger']) ?></a>
                </td>
            </tr>

        <?php endforeach; ?>
<!--        add user-->
        <?= Html::beginForm(['site/user', 'user' => $user], 'post', ['enctype' => 'multipart/form-data']) ?>
        <td>
            <?= Html::input('text',     'name',     '',['class' => $town]) ?>
        </td>
        <td >
            <?= Html::dropDownList('gender', null,
                ArrayHelper::map(Gender::find('name')->all(), 'id', 'name_gender')) ?>
        </td>
        <td>
            <?= Html::input('number',   'age',      '',['class' => $town]) ?>
        </td>
    </tbody>
    </table>
    <div class="form-group">
        <?= Html::submitButton('Додати', ['class' => 'btn btn-primary', 'data-toggle' => 'tooltip', 'title' => 'Зберегти дані нового користувача' ]) ?>
    </div>
    <?= Html::endForm() ?>

<div class="modal fade" id="myModalContacts" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Коло моїх друзів</h4>
            </div>
            <div class="modal-body">
                <li class="list-group-item list-group-item-info active" id="user">-
                </li>
                <?php  foreach($users as $user):?>
                    <li class="list-group-item list-group-item-info" id="f_category">
                        <?= $user->name ?>
                        <input id="<?= $user->id ?>" class="checked_friends" type="checkbox" />
                    </li>
                <?php   endforeach; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Вийти</button>
            </div>

        </div>

    </div>
</div>
<!--    <button href="#" data-toggle="tooltip" title="Hooray!">Hover over me</button>-->

<?php
// create tooltip
$script = <<< JS
  $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            
        });
JS;

$this->registerJs($script);



?>

<?php
$this->registerJsFile('/web/js/set_friends.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
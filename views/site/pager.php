<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
    <h1>Міста</h1>
    <ul>
        <?php foreach ($countries as $country): ?>
            <li>
                <?= Html::encode("{$country->name_town} (id: {$country->id})") ?>:
                <?= $country->country ?>
            </li>
        <?php endforeach; ?>
    </ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>
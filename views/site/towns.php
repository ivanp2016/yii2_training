<?php
use yii\helpers\Url;
use yii\widgets\Menu;
use yii\helpers\Html;
//var_dump($datas);die();
$this->title = 'Міста';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1 class="towns">Міста</h1>
    <div class="row">
    <div class="col-sm-6">
        <nav class="navbar ">
        <div class="container-fluid">
            <ul class="nav navbar-nav ">
                <li><a href="<?= Url::to(['site/towns'])?>">Сортувати по: id</a></li>
                <li><a href="<?= Url::to(['site/sort_towns'])?>">Сортувати по: місту</a></li>
                <li><a href="<?= Url::to(['site/peoples'])?>">Сортувати по: кількості жителів</a></li>
            </ul>
        </div>
        </nav>
        <?php
        echo Menu::widget([
            'items' => [
                ['label' => 'Сортувати по: id',                     'url' => ['site/towns']],
                ['label' => 'Сортувати по: місту',                  'url' => ['site/sort_towns']],
                ['label' => 'Сортувати по: кількості жителів', 'url' => ['site/peoples']],

            ],
        ]);
        ?>
        <p>
            <?= Html::a('Сортувати по: id', ['site/towns'], ['class' => 'profile-link']) ?>
        </p>
        <label  class="label label-pill label-default"><?= $label?></label>
    </div>
    <div class="col-sm-6">
        <?= Html::img('@web/images/ukraine.jpg', ['alt' => 'Наша Україна'])?>
    </div>
        <table class="table">
            <thead>
            <tr>
                <th>Країна</th>
                <th>Місто</th>
                <th>Проживає людей</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($datas as $data):?>
                <tr>
                    <td>
                        <?=$data->country?>
                    </td>
                    <td>
                        <?=$data->name_town?>
                    </td>
                    <td>
                        <?=$data->peaples?>
                    </td>
                </tr>
            <?php endforeach;?>
            <tr>
                <?= Html::beginForm(['site/form', 'town' => $town], 'post', ['enctype' => 'multipart/form-data']) ?>
                <td>
                    <?= Html::input('text', 'country', $town->name, ['class' => $town]) ?>
                </td>
                <td>
                    <?= Html::input('text', 'town', $town->town, ['class' => $town]) ?>
                </td>
                <td>
                    <?= Html::input('number', 'peoples', $town->peoples, ['class' => $town]) ?>
                </td>

            </tr>
            </tbody>
        </table>
        <?= Html::submitButton('Додати', ['class' => 'btn btn-primary']) ?>
        <?= Html::endForm() ?>
</div>

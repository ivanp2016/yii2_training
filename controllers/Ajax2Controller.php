<?php

namespace app\controllers;

use app\models\Clients;
use app\models\Frends;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Users;
use app\models\Gender;
use app\models\Friends;
use yii\helpers\Json;

class Ajax2Controller extends Controller
{
    public function actionAjax2()
    {
        $request  = Yii::$app->request;
        $id = $request->post('id');

        $friends_user = new Friends();
        $data_users = $friends_user->friendsUsers($id);
        $data_prepare = $friends_user->processorFriends($data_users);
        
        echo  Json::encode($data_prepare);
    }
    public function actionUpdateFriends()
    {
        $friends_status = new Friends();

        $request = Yii::$app->request;
        $id_user = $request->post('id_user');
        $id_friend = $request->post('id_friend');
        $status = $request->post('status');

        $exists = $friends_status->friendsCheck($id_user, $id_friend);


        if($status==1 && $exists== false)
        {
            $friends_status->saveFriend($id_user, $id_friend);
        }
        else if($status==0 && $exists== true){
            $friends_status->clearFriends($id_user, $id_friend);
        }
        
        $update_friends = new Friends();
        
        return 'user:'.$id_user.' friend:'.$id_friend.' status:'.$status;
    }

    public function actionAllUsers()
    {
        $request = Yii::$app->request;
        $empty = $request->post('empty');
        $users = new Users();
        $friend = new Friends();
        $allUsers = $users->getUsers();
        $data_prepare = $friend->processorUsers($allUsers);
        echo Json::encode($data_prepare);
    }

}

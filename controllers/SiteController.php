<?php

namespace app\controllers;

use app\models\Clients;
use app\models\Frends;
use app\models\User;
use Faker\Provider\cs_CZ\DateTime;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Town;
use app\models\Users;
use app\models\Gender;  
use app\models\Friends;
use app\models\ClientsTest;
use app\models\Clients2;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTowns()
    {
      $table = new Town();
        
        return $this->render('towns',[
            'datas' => $table->getTown(),
            'label' => 'Сортовано по id',
        ]);
    }
    public function actionPeoples()
    {
        
        $table = new Town();
        return $this->render('towns', [
            'datas' => $table->sortPeoples(),
            'label' => 'Сортовано по кількості населення',
        ]);
    }
    public function actionSort_towns()
    {
        $table = new Town();
        return $this->render('towns',[
           'datas' => $table->sortTowns(),
           'label' => 'Сортовано по місту в алфавітному порядку',
        ]);
    }
    public function actionPager()
    {
        $pager = new Town();
        $pager->pager($countries, $pagination);
        return $this->render('pager', [
            'countries' => $countries,
            'pagination' => $pagination,
        ]);

    }
    public function processor(){

    }

    public function actionFriends()
    {
        $users = new Users();
        $gender = new Gender(); 
        $friends_list = new Friends();
        $test_friend = $friends_list->getAllFriends();

        $object_users = $friends_list->arrayUsers(); //list all users
        $array_users = array();
        $array_friends = array();
        // create associative array in array with count friends to current user
        foreach ($object_users as $object_user) {
            array_push($array_users,  ["user" => $object_user->user_id,
                                    "is_friends" => $friends_list->countFriends($object_user->user_id)]);

        }
//        $name_friends = $friends_list->nameFriends(1);
//        $friends_list->proccesor();
//create friends object $array_friends
        foreach ($object_users as $object_user) {
            $temp_users = $friends_list->nameFriends($object_user->user_id);
            $prepare_users = $friends_list->processor($temp_users);

            array_push($array_friends, $prepare_users);
        }
        
        return $this->render('friends',[
            'users' => $users->getUsers(),
            'genders' =>  $gender->getGender(),
            'friends' => $friends_list->getFriends(),
            'all_friends' => $test_friend,
            'count_friends' => $array_users,
            'array_friends' => $array_friends
        ]);
    }
    public function actionForm()
    {
        $table = new Town();

        $request  = Yii::$app->request;
        $country = $request->post('country');
        $town = $request->post('town');
        $peoples = $request->post('peoples');
        $table->saveForm($country, $town, $peoples);

        return $this->redirect(Yii::$app->request->referrer);
        
    }
    public function actionDelete($id)
    {
        $delete = new Users();
        $delete->deleteUser($id);
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionEdit($id)
    {

    }

    public function actionUser()
    {
        $table = new Users();

        $request  = Yii::$app->request;
        $name =     $request->post('name');
        $gender =     $request->post('gender');
        $age =  $request->post('age');

        $table->saveNewUser($name, $gender, $age);

        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionUpdate_u()
    {
        $table = new Users();

        $request  = Yii::$app->request;
        $id =       $request->post('id');
        $name =     $request->post('name');
        $gender =     $request->post('gender');
        $age =  $request->post('age');
        $friends=   $request->post('friends');
        $gender_select = $request->post('status_gender');
//        var_dump($gender_select);die();
        $table->updateUser($id, $name, $gender, $age);

        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionParse()
    {
        function dayDifference($data)
        {
            $time = strtotime($data);
            $today = time();
            $day = ($today - $time)/86400;
            $day = floor($day);
            return $day;
        }
        
        $amount = '';
        $date = '';
        $email = '';

        if (file_exists("../web/files/new25052016.txt"))
            echo "File exist.";
        else
            echo "File not find";
        echo '</br>';
        echo filesize("../web/files/new25052016.txt")/1000 . 'Кб';
        echo '</br></br>';
        $clients = new Clients2();
        if (file_exists("../web/files/new25052016.txt")) {
            $handle = fopen("../web/files/new25052016.txt", "r"); // Открываем файл в режиме чтения
            while (!feof($handle)) {
                $buffer = (string)fgetss($handle);
//                echo $buffer.'<br>';

                $results = explode(" ", $buffer);
                foreach ($results as $key => $result) {
                    switch ($key){
                        case 0 : $amount = $result; break;
                        case 1 : $date = $result; break;
                        case 2 : $email = $result; break;
                        default: break;
                    }
                    echo $result . '</br>';
                }
                $clients->SaveClients2($amount, $date, $email, dayDifference($date));

            }

            fclose($handle);

        }


    }
}

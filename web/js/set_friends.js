$(document).ready(function(){
    var pathArray = location.href.split( '/' );
    var protocol = pathArray[0];
    var host = pathArray[2];
    var url = protocol + '//' + host;
    var id=0;

    $("button.contacts").click(function() {

        id = $(this).attr('id');
        $.ajax({
            method: 'POST',
            url: url + '/web/index.php?r=ajax2/all-users',
            data: {empty: 1},
            async: false
        })
            .done(function(data_users) {
                var users = JSON.parse(data_users);
                console.log(users);
                var j=0;
                for(j in users)
                {
                    var selector_users = 'input#'+users[j].id+'.checked_friends';
                    $(selector_users).prop('checked', false);
                    console.log(users[j].id);
                }
            });

        $.ajax({
            method: 'POST',
            url: url + '/web/index.php?r=ajax2/ajax2',
            data:   {id: id},
            async: false
        })
            .done(function(data){
                var data_ = JSON.parse(data);

                console.log(data);
                // console.log(data_[0].friend);
                // $('.checked_friends').prop('checked', false);

                var i=0;
                for(i in data_){
                    // console.log(data_[i].friend);
                    var selector_id = 'input#'+data_[i].friend+'.checked_friends';
                    $(selector_id).prop('checked', true);
                }
                var user = $("td#"+id+".users").text();
                $("li#user").text(user);
            });
    });


    $("#f_category .checked_friends").change(function(){
        var id_checkbox = $(this).attr('id');
        var status = 0;
        if ($(this).is(':checked')) {
            status = 1;
        } else {
            status = 0;
        }
        $.ajax({
            method: 'POST',
            url:    url + '/web/index.php?r=ajax2/update-friends',
            data: {id_user: id, id_friend : id_checkbox, status : status }
        })
            .done(function(data) {
                console.log(data);
            });
    });
});
    